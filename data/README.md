# README.md

## PDB files 
### AF_Q00839_F1_model_v2_pdb_ATOMS_section_With_ConSurf_isd.pdb
The PDB file was downloaded from AlphaFold (https://alphafold.ebi.ac.uk/entry/Q00839) and consists of the longer isoform (825).

### AF_Q00839_F1_model_v2_pdb_isd-coot-iso2_ATOMS_section_With_ConSurf.pdb
`AF_Q00839_F1_model_v2_pdb_ATOMS_section_With_ConSurf_isd.pdb` was modified in coot by removing the 19 aa that are not present in the shorter isoform. The modified structure was saved as `AF_Q00839_F1_model_v2_pdb_isd-coot-iso2_ATOMS_section_With_ConSurf.pdb` and used as the input structure for analyses.

## gnomAD_v2.1.1_ENST00000444376_2023_03_29_11_40_00.csv
The missense variant data was downloaded from the gnomAD database (v2.1.1) for hnRNPU splice variant ENST00000444376.2 (806 aa):
http://www.gnomad-sg.org/transcript/ENST00000444376?dataset=gnomad_r2_1

## Sequence alignment
### hnrnpu_806_refined.fasta 
This FASTA file contains all of the hnRNPU sequences that were included in the alignment. 

### hnrnpu_806_refined_aln.fasta 
This FASTA file contains the aligned hnRNPU sequences from `hnrnpu_806_refined.fasta`.
 
### hnrnpu_806_refined_aln_trimmed.fasta
This file is a manually trimmed version of `hnrnpu_806_refined_aln.fasta`. Additional sequences were added to the alignment to include more sequences from the shorter isoform (806 aa). The trimmed alignment was submitted to Consurf with the AlphaFold structure.

### plot_protein_hnRNPU_domainfile.txt
This file contains the domain boundary coordinates of the SAP, SPRY, and PNK domains. The domain coordinates for SPRY and PNK were adjusted by subtracting 19 aa (as the coordinates were taken from the longer isoform). 

The script expects a tabluar format with column names:
```
architecture_name	start_site	end_site	colour
SAP	23	42	"#00AACC"
SPRY	236	448	"#007A99"
PNK	449	639	"#33E4FF"

```

## plot_protein_hnRNPU_motifs.txt
This file contains the coordinates for the RGG motif. 

The script expects a tabular format with column names:
```
Motif	Start	End
RGG	683	685
RGG	690	692
RGG	696	698
...
```

## plot_protein_hnRNPU_PTM.txt
This file contains the phosphorylation sites taken from UniProt, all sites after 212 had their position subtracted by 19 to account for the coordinates being associated with the longer isoform. 

The file structure consists of a column name followed by the coordinate of each PTM:
```
site
2
3
4
...
```

## plot_protein_hnRNPU_secondary_str.txt
This file contains the alpha and beta domain boundaries, these were aquired by manually checking the coordinates for secondary structures in the AlphaFold model. 

The script expects a comma separated file  with column names:
```
arch_name,start,end
alpha,8,10
alpha,13,23
alpha,31,47
...
```

## VdVp_hnRNPU_domains.txt
As the longer isoform (825 aa) contains an extra 19 aa after 212, 19 is subtracted from the coordinates for the SPRY (256-467) and PNK (468-658) domains.

The structure of the `VdVp_hnRNPU_domains.txt` file:
```
SAP,23,42			
SPRY,237,448
PNK,449,639
```
