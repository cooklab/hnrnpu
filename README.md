# hnRNPU analysis

This repository contains the scripts and data used to generate the hnRNPU analysis results. 

## data
The directory contains the input files used by the scripts in `src`:
- AF_Q00839_F1_model_v2_pdb_ATOMS_section_With_ConSurf_isd.pdb
- AF_Q00839_F1_model_v2_pdb_isd-coot-iso2_ATOMS_section_With_ConSurf.pdb
- gnomAD_v2.1.1_ENST00000444376_2023_03_29_11_40_00.csv
- hnrnpu_806_refined.fasta
- hnrnpu_806_refined_aln.fasta
- hnrnpu_806_refined_aln_trimmed.fasta
- plot_protein_hnRNPU_domainfile.txt
- plot_protein_hnRNPU_motifs.txt
- plot_protein_hnRNPU_PTM.txt
- plot_protein_hnRNPU_secondary_str.txt
- VdVp_hnRNPU_domains.txt

## results
The directory contains the output files produced by the scripts in `src `:
- 1Dto3D_hnRNPU_script.txt
- gnomAD_hnRNPU_filtered.csv
- hnRNPU_protein_plot.svg
- PDB_hnRNPU_scores.txt
- plot_protein_converter_hnRNPU_output.tsv
- VdVp_hnRNPU_results.txt

## src
The directory contains the scripts used to generate the hnRNPU analysis results:
- 1D_to_3D.py
- PDB_score_converter.py
- Plot_Protein_converter.py
- plot_protein_hnRNPU_modified.R
- VdVp_Calculator.py
