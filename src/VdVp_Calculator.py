#!/usr/bin/env python

#Vd/Vp Ratio Calculator

#This program contains four parts:
#1. Setup
#2. Filtering GnomAD Data
#3. Calculating the Vp and Vd(s)
#4. Calculating & Exporting Vd/Vp Ratios


#---------------------------------1. Setup---------------------------------
#Imports required modules
import pandas as pd
import numpy as np
import sys
import argparse

#Defines command line arguments

parser = argparse.ArgumentParser()
parser.add_argument('-g',
                    '--gnomad_csv',
                    dest='gnomad_csv',
                    required=True,
                    help='Path to GnomAD csv file',
                    default=None)
parser.add_argument('-l',
                    '--length',
                    dest='length',
                    required=True,
                    type=int,
                    help='Length of protein',
                    default=None)
parser.add_argument('-d',
                    '--domain_file',
                    dest='domain_file',
                    required=True,
                    help='Path to domain file')
parser.add_argument('-s',
                    '--save',
                    dest='save',
                    required=True,
                    help='Path to save output file',
                    default=None)
args = parser.parse_args()


#-------------------------2. Filtering GnomAD Data-------------------------


def filter_data(gnomad_csv):
    #Converts gnomad csv file to dataframe
    gnomad_frame = pd.read_csv(gnomad_csv, sep=None, engine='python')

    #Selects relevant columns and renames them with underscores
    gnomad_frame = gnomad_frame[['Position','rsIDs','Filters - exomes',\
                                 'Protein Consequence','VEP Annotation',\
                                 'ClinVar Clinical Significance',\
                                 'Allele Frequency']]
    cols = ['Position','rsID','Filters_exomes','Protein_Consequence',\
            'VEP_Annotation','ClinVar_Clinical_Significance','Allele_Frequency']
    gnomad_frame.columns = cols

    #Filters for missense variants from exome sequencing data
    gnomad_frame =gnomad_frame[gnomad_frame['VEP_Annotation']=='missense_variant']
    gnomad_frame =gnomad_frame[gnomad_frame['Filters_exomes']=='PASS']
    gnomad_frame = gnomad_frame.drop(['VEP_Annotation','Filters_exomes'],axis=1)

    #Filters out potentially pathogenic variants
    ClinVar = ['Uncertain significance','Likely pathogenic','Pathogenic',\
               'Conflicting interpretations of pathogenicity']
    gnomad_frame = gnomad_frame[~gnomad_frame.ClinVar_Clinical_Significance.\
                                isin(ClinVar)]
    gnomad_frame = gnomad_frame.drop(['ClinVar_Clinical_Significance'],axis=1)

    #Extracts residue numbers of variants from the Protein_Consequence column
    gnomad_frame = gnomad_frame.Protein_Consequence.str.extract('(\d+)')
    gnomad_frame.columns = ['Res_Num']
    gnomad_frame['Res_Num'] = pd.to_numeric(gnomad_frame['Res_Num'])
    return gnomad_frame

data = filter_data(args.gnomad_csv)


#---------------------3. Calculating the Vp and Vd(s) ---------------------


#Gets user-defined protein length and variant residue numbers
protein_length = args.length
protein_variants = data.to_numpy()

#Calculates the Vp
def calculator(variants, length):
    (unique, counts) = np.unique(variants, return_counts=True)
    return len(counts)/int(length)

Vp = calculator(protein_variants,protein_length)

#Gets user-defined domain file and creates dataframe
domain_file = args.domain_file
domain_frame = pd.read_csv(domain_file, sep=",", header=None)
domain_frame.columns = ['Name','Start','End']
domain_frame[['Start','End']] = domain_frame[['Start','End']].apply(pd.to_numeric)

#Generates lists for domain lengths and boundaries
domain_lengths = list(domain_frame['End']-domain_frame['Start']+1)
domain_start = list(domain_frame['Start'])
domain_end = list(domain_frame['End'])
domain_names = list(domain_frame['Name'])

#Calculates the Vd(s)

Vd = []

for start,end,length in zip(domain_start,domain_end,domain_lengths):
    variants = data[(data['Res_Num'] >= start) & (data['Res_Num'] <= end)]
    (unique, counts) = np.unique(variants, return_counts=True)
    Vd.append(len(counts)/length)


#------------------4.Calculating & Exporting Vd/Vp Ratios------------------


#Gets user path to save result file 
results_path = args.save

#Calculates Vd/Vp ratios and writes results into a new file
og_stdout = sys.stdout

with open(results_path,'w') as results:
    sys.stdout = results
    print('Vp_protein\t'+str(round(Vp,2)))
    for name,domain_vd in zip(domain_names,Vd):
        VdVp = domain_vd/Vp
        print('VdVp_'+name+'\t'+str(round(VdVp,2)))
    sys.stdout = og_stdout

#Prints echo message
print('Your job has been completed! Please check the destination file.')

#End.



