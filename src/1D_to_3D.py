#!/usr/bin/env python

#1D-to-3D

#This program contains four parts:
#1. Setup 
#2. Filtering GnomAD Data
#3. Domain-Specific Data Processing
#4. Generating a PyMOL Script


#---------------------------------1. Setup---------------------------------


#Imports required modules
import pandas as pd
import numpy as np
import sys
import argparse

#Defines command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-g',
                    '--gnomad_csv',
                    dest='gnomad_csv',
                    required=True,
                    help='Path to GnomAD csv file',
                    default=None)
parser.add_argument('-c',
                    '--save_csv',
                    dest='save_csv',
                    help='Path for saving processed data as a new csv file',
                    default=None)
parser.add_argument('-f',
                    '--first_res',
                    dest='first_res',
                    required=True,
                    type=int,
                    help='First residue number of the structure')
parser.add_argument('-l',
                    '--last_res',
                    dest='last_res',
                    required=True,
                    type=int,
                    help='Last residue number of the structure')
parser.add_argument('-n',
                    '--sel_name',
                    dest='sel_name',
                    required=True,
                    help='Name of your PyMOL selection',
                    default=None)
parser.add_argument('-p',
                    '--py_num',
                    dest='py_num',
                    type=int,
                    help='First residue number of the PyMOL selection.\
                    Ignore if residue numbering is biologically correct.')
parser.add_argument('-t',
                    '--true_num',
                    dest='true_num',
                    type=int,
                    help='True first residue number of the PyMOL selection.\
                    Ignore if residue numbering is biologically correct.',
                    default=None)
parser.add_argument('-s',
                    '--save_script',
                    dest='save_script',
                    required=True,
                    help='Path to save the PyMOL script as a txt file.',
                    default=None)
parser.add_argument('-ch',
                    '--chain',
                    dest='chain',
                    help='Chain to be used for your PyMOL selection',
                    default='A')
args = parser.parse_args()


#-------------------------2. Filtering GnomAD Data-------------------------


def filter_data(gnomad_csv,save_csv):
    #Converts gnomad csv file to dataframe
    gnomad_frame = pd.read_csv(gnomad_csv, sep=None, engine='python')

    #Selects relevant columns and renames them with underscores
    gnomad_frame = gnomad_frame[['Position','rsIDs','Filters - exomes',\
                                 'Protein Consequence','VEP Annotation',\
                                 'ClinVar Clinical Significance',\
                                 'Allele Frequency']]
    cols = ['Position','rsID','Filters_exomes','Protein_Consequence',\
            'VEP_Annotation','ClinVar_Clinical_Significance','Allele_Frequency']
    gnomad_frame.columns = cols

    #Filters for missense variants from exome sequencing data
    gnomad_frame = gnomad_frame[gnomad_frame['VEP_Annotation']=='missense_variant']
    gnomad_frame = gnomad_frame[gnomad_frame['Filters_exomes']=='PASS']
    gnomad_frame = gnomad_frame.drop(['VEP_Annotation','Filters_exomes'],axis=1)

    #Filters out potentially pathogenic variants
    ClinVar = ['Uncertain significance','Likely pathogenic','Pathogenic',\
               'Conflicting interpretations of pathogenicity']
    gnomad_frame = gnomad_frame[~gnomad_frame.ClinVar_Clinical_Significance.\
                                isin(ClinVar)]
    gnomad_frame = gnomad_frame.drop(['ClinVar_Clinical_Significance'],axis=1)

    #Function that slices the 'Protein_Consequence' column
    #e.g.: "p.Ala589Thr" to "Ala","589", and "Thr" ("p." is discarded)
    def slicer(df):
        df['slice1'] = df['Protein_Consequence'].str.slice(stop=2)
        df['slice2'] = df['Protein_Consequence'].str.slice(start=2)
        df['Original_Res'] = df['slice2'].str.slice(stop=3)
        df['slice3'] = df['slice2'].str.slice(start=3)
        df['Res_Num'] = df['slice3'].str.slice(stop=-3)
        df['New_Res'] = df['slice3'].str.slice(start=-3)

    slicer(gnomad_frame)

    #Reorganizes the data frame into four columns in appropriate order
    gnomad_frame = gnomad_frame.drop(['Protein_Consequence','slice1','slice2',\
                                      'slice3'],axis=1)
    new_order = ['Position','rsID','Original_Res','Res_Num','New_Res',\
                 'Allele_Frequency']
    gnomad_frame = gnomad_frame.reindex(columns=new_order)

    #Saves processed data as a new csv file if path is supplied

    if (save_csv):
        gnomad_frame.to_csv(save_csv,index = False)
    return gnomad_frame

gnomad_frame = filter_data(args.gnomad_csv,args.save_csv)


#--------------------3. Domain-Specific Data Processing--------------------


def process_data(gnomad_frame):
    #Reduces the dataframe and converts residue numbers to integers
    gnomad_frame = gnomad_frame.drop(['Position','rsID',\
                                      'Original_Res','New_Res'],axis=1)
    gnomad_frame['Res_Num'] = pd.to_numeric(gnomad_frame['Res_Num'])

    #Creates a new dataframe based on user-defined residue range 
    domain_df = gnomad_frame[(gnomad_frame['Res_Num'] >= args.first_res) & \
                             (gnomad_frame['Res_Num'] <= args.last_res)]

    #Gets sums of allele frequencies per residue, multiplies them by 10^6,
    #and normalises with log base 10

    domain_df = domain_df.groupby('Res_Num')['Allele_Frequency'].sum().\
                to_frame().reset_index()
    domain_df['Allele_Frequency'] = domain_df['Allele_Frequency']*(10**6)
    domain_df['log_score'] = np.log10(domain_df['Allele_Frequency'])

    #Sorts variants in the domain into PyMOL selections s1-s6 based
    #on their "log_scores" 

    s1=domain_df.query('log_score <= 1')
    s2=domain_df.query('log_score > 1 & log_score <= 2')
    s3=domain_df.query('log_score > 2 & log_score <= 3')
    s4=domain_df.query('log_score > 3 & log_score <= 4')
    s5=domain_df.query('log_score > 4 & log_score <= 5')
    s6=domain_df.query('log_score > 5 & log_score <= 6')

    #Creates a list of residue numbers for each selection
    s1=s1['Res_Num'].tolist()
    s2=s2['Res_Num'].tolist()
    s3=s3['Res_Num'].tolist()
    s4=s4['Res_Num'].tolist()
    s5=s5['Res_Num'].tolist()
    s6=s6['Res_Num'].tolist()

    #Processes each list into printable format
    s1=','.join([str(elm) for elm in s1])
    s2=','.join([str(elm) for elm in s2])
    s3=','.join([str(elm) for elm in s3])
    s4=','.join([str(elm) for elm in s4])
    s5=','.join([str(elm) for elm in s5])
    s6=','.join([str(elm) for elm in s6])

    return s1,s2,s3,s4,s5,s6

s1,s2,s3,s4,s5,s6 = process_data(gnomad_frame)


#-----------------------4. Generating a PyMOL Script-----------------------


def write_script(s1,s2,s3,s4,s5,s6):

    #Writes PyMOL script in a new txt file
    og_stdout = sys.stdout
  
    with open(args.save_script,'w') as pymol_script:
        sys.stdout = pymol_script

        # Part 1: sets display settings
        print('bg_color white')
        print('select '+str(args.sel_name)+', chain '+str(args.chain))
        print('color white, '+str(args.sel_name))
        print('util.performance(0)')
        print('space rgb')
        print('set ray_shadows,off')

        # Part 2: renumbers residues in PyMOL file if specified
        if args.py_num:
            if args.true_num >= 0:
                print('alter '+str(args.sel_name)+','+' resi=str(int(resi)-'+\
                      str(args.py_num)+')')
                print('alter '+str(args.sel_name)+','+' resi=str(int(resi)+'+\
                      str(args.true_num)+')')
            else:
                print('alter '+str(args.sel_name)+','+' resi=str(int(resi)-'+\
                      str(args.py_num)+')')
                print('alter '+str(args.sel_name)+','+' resi=str(int(resi)+'+\
                      str(args.true_num+1)+')')

        # Part 3: selects variants and assigns sphere size + color based on log scores
        print('select s1, (((i;'+str(s1)+') and n; CA) and '+str(args.sel_name)+')')
        print('select s2, (((i;'+str(s2)+') and n; CA) and '+str(args.sel_name)+')')
        print('select s3, (((i;'+str(s3)+') and n; CA) and '+str(args.sel_name)+')')
        print('select s4, (((i;'+str(s4)+') and n; CA) and '+str(args.sel_name)+')')
        print('select s5, (((i;'+str(s5)+') and n; CA) and '+str(args.sel_name)+')')
        print('select s6, (((i;'+str(s6)+') and n; CA) and '+str(args.sel_name)+')')
        print('show spheres, (s1,s2,s3,s4,s5,s6)')
        print('select m1, ((resid '+str(s1)+') and chain '+str(args.chain)+')')
        print('select m2, ((resid '+str(s2)+') and chain '+str(args.chain)+')')
        print('select m3, ((resid '+str(s3)+') and chain '+str(args.chain)+')')
        print('select m4, ((resid '+str(s4)+') and chain '+str(args.chain)+')')
        print('select m5, ((resid '+str(s5)+') and chain '+str(args.chain)+')')
        print('select m6, ((resid '+str(s6)+') and chain '+str(args.chain)+')')
        log_scores = ('s1','s2','s3','s4','s5','s6')
        surface_scores = ('m1','m2','m3','m4','m5','m6')
        sphere_scales = ('1.15','1.50','1.85','2.15','2.50','2.85')
        blues = ('b1, [100,120,250]','b2, [35,40,200]','b3, [18,0,150]','b4, [9,0,100]','b5, [0,0,50]','b6, [0,0,0]')
        blue_names = ('b1','b2','b3','b4','b5','b6')
        for log_score,sphere_scale in zip(log_scores,sphere_scales):
            print('set sphere_scale, '+sphere_scale+', '+log_score)
        for blue in blues:
            print('set_color '+blue)
        for log_score,name in zip(log_scores,blue_names):
            print('color '+name+', '+log_score)
        for surface_score,name in zip(surface_scores,blue_names):
            print('color '+name+', '+surface_score)
        sys.stdout = og_stdout

write_script(s1,s2,s3,s4,s5,s6)

#Prints echo message
print('Your job has been completed! Please check the destination file.')

#End.

