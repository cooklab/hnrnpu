#!/usr/bin/env python

# PDB file converter

# This program processes a pdb file with consurf scores and 
# converts them into a consurf score file for the R-based application
# Plot Protein by Turner, 2013. It contains three parts:

#1. Setup
#2. Process PDB file
#3. Generating Consurf Score file



#-----------------------------------1. Setup-------------------------------------------------

#Import required modules
import pandas as pd
import numpy as np
import argparse

#Define command line arguments

parser = argparse.ArgumentParser()
parser.add_argument('-p',
                    '--consurf_pdb',
                    dest='consurf_pdb',
                    required=True,
                    help='Path to Consurf PDB file',
                    default=None)
parser.add_argument('-c',
                   '--chain',
                   dest='chain',
                   required=True,
                   help='Chain assigned to in your PDB file',
                   default=None)
parser.add_argument('-s',
                    '--save',
                    dest='save',
                    required=True,
                    help='Path to save output file',
                    default=None)
args = parser.parse_args()



#-----------------------------------2. Processing PDB file------------------------------------

def process_PDB(consurf_pdb, chain):

        #Define column names
        column_names = ["atom","atom_pos","atm","res","chain","res_pos","X","Y","Z","occupancy","score"]
        #pdb files processed by coot have an additional column"
        coot_column_names = ["atom","atom_pos","atm","res","chain","res_pos","X","Y","Z","occupancy","score","atmid"]
        
        #Open consurf PDB file
        with open(consurf_pdb,'r') as file:
            data = file.readlines()

        #Process PDB file by filtering for lines containing 'ATOM', removing linebreaks, 
        #splitting merged columns (this happens after res_pos 999) and removing empty spaces,
        temp_atomline = list()
        temp_atom_file = list()
        for line in data:
            if 'ATOM' in line:
                line = line.strip("\n").replace(str(chain)+str(1), str(chain)+' '+str(1))
                temp_atomline = line.split(" ")
                #add line to temporary variable, remove empty spcaes
                temp_atom_file.append(list(filter(None, temp_atomline)))
        
        #Convert into pandas Dataframe
        df = pd.DataFrame(temp_atom_file)
        #define number of columns and assign column names for appropriate pdb file
        col = df.columns
        if len(col) == len(column_names):
          df = df.set_axis(column_names, axis=1)
        elif len(col) == len(coot_column_names):
          df = df.set_axis(coot_column_names, axis=1)

        #Filter for columns of interest and remove duplicated rows
        dataframe = df[["res","res_pos","score"]].drop_duplicates()

        return dataframe

dataframe = process_PDB(args.consurf_pdb, args.chain)




#-----------------------------------3. Saving processed file----------------------------------



#Get user path to save result
results_path = args.save
np.savetxt(results_path,dataframe.values,fmt='%s',delimiter='\t')

#Prints echo message
print('Your job has been completed! Please check the destination file.')

#End.
