# README.md
The README outlines the use of the scripts contained within this directory. This includes usage, which inputs files are required and which output files are generated. 

### 1D_to_3D.py
A modified version of `1D_to_3D.py` (from https://git.ecdf.ed.ac.uk/cooklab/deak) was run with the gnomAD data as input, with `gnomAD_hnRNPU_filtered.csv` and `hnRNPU_script.txt` produced as output files. 

Usage:
```
python 1D_to_3D.py \
    -g gnomAD_v2.1.1_ENST00000444376_2023_03_29_11_40_00.csv \
    -c gnomAD_hnRNPU_filtered.csv \
    -f 1 \
    -l 806 \
    -n hnRNPU \
    -s 1Dto3D_hnRNPU_script.txt
```

### PDB_score_converter.py
To generate the input file `hnRNPU_scores.txt` the `PDB_score_converter.py` script was run, which takes a PDB file with Consurf scores as input and produces the output file `hnRNPU_scores.txt`.

Usage:
```
python PDB_score_converter.py \
    -p AF_Q00839_F1_model_v2_pdb_ATOMS_section_With_ConSurf_isd-coot-iso2.pdb \
    -c A \
    -s PDB_hnRNPU_scores.txt
```

### Plot_Protein_Converter.py
The input data required for the R script `Plot_protein_converter_hnRNPU.R` are produced by running the `Plot_Protein_Converter.py` script. This script requires the gnomAD data as input and produces `hnRNPU_protein_converter_output.txt` as its output. 

Usage:
```
python Plot_Protein_Converter.py \
    -g gnomAD_v2.1.1_ENST00000444376_2023_03_29_11_40_00.csv \
    -n hnRNPU \
    -p Q00839-2 \
    -s plot_protein_converter_hnRNPU_output.tsv  
```

### VdVp_Calculator.py
The `VdVp_Calculator.py` script (sourced from https://git.ecdf.ed.ac.uk/cooklab/deak) was run with the gnomAD data (not filtered) and `hnRNPU_domains.txt` as input, with `hnRNPU_VdVp_results.txt` produced as output. 

Usage:
```
python VdVp_Calculator.py \
    -g gnomAD_v2.1.1_ENST00000444376_2023_03_29_11_40_00.csv \
    -l 806 \
    -d VdVp_hnRNPU_domains.txt \
    -s VdVp_hnRNPU_results.txt
```

### Plot_protein_hnRNPU_modified.R
Files that need to be made manually before running `plot_protein_hnRNPU_modified.R`:
-	`plot_protein_hnRNPU_domainfile.txt` – domain boundaries. Domain coordinates for SPRY and PNK were adjusted by subtracting 19 aa. 
-	`plot_protein_hnRNPU_PTM.txt` – phosphorylation sites. This file consists of phosphorylation sites taken from the UniProt webpage for the protein, all sites after 212 had their position subtracted by 19 to account for the longer isoform. 
-	`plot_protein_hnRNPU_secondary_str.txt` – alpha and beta domain boundaries. Manually checked coordinates for secondary structures in the AlphaFold model.
  
Other files made earlier (above) are used as well:
-	`plot_protein_converter_hnRNPU_output.tsv`
-	`PDB_hnRNPU_scores.txt`
-	`VdVp_hnRNPU_results.txt`

Running the script produces an SVG file of the figure – `hnRNPU_protein_plot.svg`.
